# Sukoe  Main
Main Repository for Sukoe.com

This repository contain the main API for <a href="https://www.sukoe.com/en">Sukoe</a> Website

<a href="https://www.sukoe.com/en">Technology Solutions for the Pharmaceutical Sector</a>  
Tailor-Made Software  
Artificial Intelligence  
Technology Platform  

You can see an example in <a href="https://www.sukoe.com/en/machine-learning.html">Sukoe Artificial Intelligence</a>

--------

Repositorio principal de Sukoe.com

Este repositorio continene el API principal de <a href="https://www.sukoe.com">Sukoe</a> Website

<a href="https://www.sukoe.com">Soluciones Tecnológicas para el Sector Farmacéutico</a>  
Software a Medida  
Inteligencia Artificial  
Plataforma Tecnológica  

Puedes ver un ejemplo en <a href="https://www.sukoe.com/machine-learning.html">Sukoe Inteligencia Artificial</a>
